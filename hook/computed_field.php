<?php
declare(strict_types=1);

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Field\FieldItemInterface;
use function hook_computed_field_dispatcher_computed_field_compute as compute;
use function hook_computed_field_dispatcher_computed_field_format as format;

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface $entity
 * @param array $fields
 * @param int $delta
 *
 * @return mixed
 */
function computed_field_shareholding_ratio_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'shareholding_ratio';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed $value_raw
 * @param string $value_escaped
 * @param FieldItemInterface $field_item
 * @param int $delta
 * @param string $langcode
 *
 * @return string
 */
function computed_field_shareholding_ratio_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
): string {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}
