<?php
declare(strict_types=1);

namespace Drupal\site_custom\Filter\Base;

use Drupal;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * @class FilterItemBase
 */
abstract class FilterItemBase
{
  use StringTranslationTrait;

  protected $key;

  protected $value;

  /**
   * @param string $key
   */
  public function __construct(string $key)
  {
    $this->key = $key;
  }

  /**
   * @return array
   */
  abstract public function element(): array;

  abstract public function getItem();

  /**
   * @return mixed
   */
  public function getValue()
  {
    return $this->value;
  }

  /**
   * @param mixed $default
   *
   * @return mixed
   */
  protected function query($default)
  {
    return Drupal::request()->query->get($this->key, $default);
  }
}
