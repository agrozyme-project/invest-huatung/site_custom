<?php
declare(strict_types=1);

namespace Drupal\site_custom\Filter\Base;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Illuminate\Support\Collection;

/**
 * @class FilterBase
 */
abstract class FormFilterBase
{
  use StringTranslationTrait;

  protected $items;

  public function __construct()
  {
    $this->items = $this->setupItems();
  }

  /**
   * @return Collection
   */
  abstract protected function setupItems(): Collection;

  /**
   * @return array
   */
  public function build(): array
  {
    $items = $this->getItems()
      ->map(function ($item, $key) {
        /**
         * @var FilterItemBase $item
         */
        return $item->element();
      })
      ->toArray();

    return ['#type' => 'details', '#title' => $this->t('Filters')] + $items;
  }

  /**
   * @return Collection
   */
  public function getItems(): Collection
  {
    return $this->items;
  }
}
