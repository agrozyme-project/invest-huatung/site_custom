<?php
declare(strict_types=1);

namespace Drupal\site_custom\Filter;

use Drupal\site_custom\Filter\Base\FilterItemBase;

/**
 * @class Year
 */
class Year extends FilterItemBase
{
  public function element(): array
  {
    return [
      '#type' => 'range',
      '#title' => $this->t('Year'),
      '#default_value' => $this->value,
      '#min' => 2020,
      '#max' => date('Y'),
    ];
  }

  /**
   * @return int
   */
  public function getItem(): int
  {
    return $this->value;
  }
}
