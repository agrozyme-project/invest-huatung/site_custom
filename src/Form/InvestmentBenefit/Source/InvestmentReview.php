<?php
declare(strict_types=1);

namespace Drupal\site_custom\Form\InvestmentBenefit\Source;

use Drupal;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * @class InvestmentReview
 */
class InvestmentReview
{
  protected $end;
  protected $start;

  public function __construct(DrupalDateTime $start, DrupalDateTime $end)
  {
    $this->start = $start;
    $this->end = $end;
  }

  /**
   * @return QueryInterface
   */
  public function getQuery(): QueryInterface
  {
    $field = 'second_review.entity:paragraph.date';
    $format = DateTimeItemInterface::DATETIME_STORAGE_FORMAT;
    return Drupal::entityQuery('node')
      ->condition('type', 'investment_review')
      ->condition('status', 1)
      ->condition($field, $this->start->format($format), '>=')
      ->condition($field, $this->end->format($format), '<');
  }
}
