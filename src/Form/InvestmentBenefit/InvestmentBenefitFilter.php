<?php
declare(strict_types=1);

namespace Drupal\site_custom\Form\InvestmentBenefit;

use Drupal\site_custom\Filter\Base\FormFilterBase;
use Illuminate\Support\Collection;

/**
 * @class InvestmentBenefitFilter
 */
class InvestmentBenefitFilter extends FormFilterBase
{
  protected function setupItems(): Collection
  {
    $items = [
      'date' => new Date('date'),
    ];

    return Collection::make($items);
  }
}
