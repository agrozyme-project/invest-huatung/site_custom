<?php
declare(strict_types=1);

namespace Drupal\site_custom\Form\InvestmentBenefit;

use Drupal\site_custom\Filter\Base\FormFilterBase;
use Drupal\site_custom\Form\Base\ReportFormBase;
use Drupal\site_custom\Report\Base\ReportBase;
use Drupal\site_custom\Report\InvestmentBenefit\InvestmentBenefitReport;

/**
 * @class InvestmentBenefitForm
 */
class InvestmentBenefitForm extends ReportFormBase
{
  public function getFormId(): string
  {
    return 'InvestmentBenefitForm';
  }

  protected function getFilter(): FormFilterBase
  {
    return new InvestmentBenefitFilter();
  }

  /**
   * @param $filter
   *
   * @return ReportBase
   */
  protected function getReport($filter): ReportBase
  {
    return new InvestmentBenefitReport($filter);
  }
}
