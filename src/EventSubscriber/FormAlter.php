<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber;

use Drupal\core_event_dispatcher\Event\Form\FormAlterEvent as Event;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @class FormAlter
 */
class FormAlter implements EventSubscriberInterface
{
  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array
  {
    return [HookEventDispatcherInterface::FORM_ALTER => 'alter'];
  }

  /**
   * @param Event $event
   */
  public function alter(Event $event)
  {
    //dpm($event->getFormId());
    //$form = &$event->getForm();
    //$form['#attributes']['novalidate'] = true;
  }
}
