<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\ComputedField\Node;

use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\computed_field_dispatcher\EventSubscriber\ComputedFieldEventSubscriberBase as Base;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\site_custom\Helper\BigNumber;
use Drupal\site_custom\Helper\FieldReader;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @class ShareholdingRatio
 */
class ShareholdingRatio extends Base implements EventSubscriberInterface
{
  protected $shareholding_quantity = 0;
  protected $total_share = 0;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array
  {
    return [
      'hook_event_dispatcher.computed_field.node.shareholding_ratio.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.shareholding_ratio.format' => 'format',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function compute(ComputeEvent $computeEvent): bool
  {
    $scale = static::SCALE;

    if (false === parent::compute($computeEvent)) {
      $computeEvent->setValue('');
      return false;
    }

    $total_share = BigNumber::create($this->total_share, $scale);

    if ($total_share->isZero()) {
      $computeEvent->setValue('');
      return false;
    }

    $value = BigNumber::create($this->shareholding_quantity, $scale)->div($total_share);
    $computeEvent->setValue($value->__toString());
    return true;
  }

  /**
   * {@inheritdoc}
   */
  protected function getValues(ComplexDataInterface $data): array
  {
    $fields = [
      FieldReader::create($data, 'shareholding_quantity', 'value'),
      FieldReader::create($data, 'total_share', 'value'),
    ];

    return FieldReader::getValues($fields);
  }
}
