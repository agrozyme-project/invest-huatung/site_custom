<?php
declare(strict_types=1);

namespace Drupal\site_custom\EventSubscriber\FormIdAlter;

use Drupal\core_event_dispatcher\Event\Form\FormIdAlterEvent as Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @class ViewsExposedForm
 */
class ViewsExposedForm implements EventSubscriberInterface
{
  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array
  {
    return [
      'hook_event_dispatcher.form_views_exposed_form.alter' => 'alter',
    ];
  }

  /**
   * @param Event $event
   */
  public function alter(Event $event)
  {
    $form = &$event->getForm();
    $form['#theme_wrappers']['details'] = ['#title' => t('Filters')];
    $form['#attached']['library'][] = 'site_custom/views-exposed-form-filter';

    $keys = array_filter(array_keys($form), function ($item) {
      return is_string($item) && '' !== $item && '#' !== $item[0];
    });

    foreach ($keys as $key) {
      $item = &$form[$key];

      if (isset($item['#type']) && 'select' === $item['#type']) {
        $item['#chosen'] = true;
      }

      if (isset($item['min']) && isset($item['max']) && isset($item['#tree']) && $item['#tree']) {
        $item['#theme_wrappers']['fieldset'] = [
          '#title' => $item['min']['#title'],
          '#type' => 'details',
        ];
        $item['#theme_wrappers']['fieldset']['#attributes']['class'][] = 'container-inline';
        $item['min']['#title'] = '';
      }
    }
  }
}
