<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\Base;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Illuminate\Support\Collection;
use Litipk\BigNumbers\Decimal;
use Litipk\BigNumbers\DecimalConstants;

/**
 * @class TableReportBase
 */
abstract class TableReportBase extends ReportBase
{
  protected $rows = [];

  /**
   * @return array
   */
  public function getContent(): array
  {
    $header = $this->getTableHeader();
    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $this->getTableRows(array_keys($header)),
    ];
  }

  /**
   * @return TranslatableMarkup[]
   */
  abstract public function getTableHeader(): array;

  /**
   * @param array $keys
   *
   * @return array
   */
  public function getTableRows(array $keys): array
  {
    $rows = [];
    $options = $this->getTableOptions();

    foreach ($options as $option) {
      $row = [];

      foreach ($keys as $key) {
        $row[] = $option[$key] ?? '';
      }

      $rows[] = $row;
    }

    return $rows;
  }

  /**
   * @return array
   */
  abstract public function getTableOptions(): array;

  /**
   * @param Collection $items
   *
   * @return array
   */
  protected function wrapperTableOptions(Collection $items): array
  {
    $rows = [];
    $data = [];
    $count = DecimalConstants::zero();

    $items->each(function ($item, $key) use (&$rows, &$data, &$count) {
      $row = $this->getTableRow($item);
      $rows[] = $row;
      $data[] = $this->wrapperTableRow($row, $key, $count);
    });

    $this->rows = $rows;
    return $data;
  }

  /**
   * @param $item
   *
   * @return RowBase
   */
  abstract protected function getTableRow($item): RowBase;

  /**
   * @param RowBase $row
   * @param         $key
   * @param Decimal $count
   *
   * @return string[]
   */
  protected function wrapperTableRow(RowBase $row, $key, Decimal &$count): array
  {
    $count = $count->add(DecimalConstants::one());
    $data = $row->formatFields();
    $data['#'] = strval($count);
    return $data;
  }
}
