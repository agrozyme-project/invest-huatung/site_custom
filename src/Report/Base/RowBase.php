<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\Base;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\site_custom\Helper\Field\Base\FieldBase;

/**
 * @class RowBase
 */
abstract class RowBase
{
  protected $dataSource;

  protected $fields = [];

  /**
   * @return FormattableMarkup[]
   */
  public function formatFields(): array
  {
    $items = [];

    foreach ($this->fields as $index => $item) {
      $items[$index] = new FormattableMarkup($item->toString(), []);
    }

    return $items;
  }

  /**
   * @return FieldBase[]
   */
  public function getFields(): array
  {
    return $this->fields;
  }

  /**
   * @return FieldBase[]
   */
  abstract protected function setupFields(): array;
}
