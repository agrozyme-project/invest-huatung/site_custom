<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\Base;

use Drupal\site_custom\Filter\Base\FormFilterBase;

/**
 * @class ReportBase
 */
abstract class ReportBase
{
  protected $filter;

  /**
   * @param FormFilterBase $filter
   */
  public function __construct(FormFilterBase $filter)
  {
    $this->filter = $filter;
  }

  /**
   * @return array
   */
  abstract public function getContent(): array;
}
