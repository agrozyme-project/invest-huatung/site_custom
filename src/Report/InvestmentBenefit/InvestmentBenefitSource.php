<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\InvestmentBenefit;

use DateInterval;
use Drupal;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\Entity\Node;
use Exception;

/**
 * @class InvestmentBenefitSource
 */
class InvestmentBenefitSource
{
  protected $end;
  protected $quarter;
  protected $start;
  protected $year;

  public function __construct(int $year, int $quarter)
  {
    $this->year = $year;
    $this->quarter = $quarter;
    $range = $this->getQuarterRange($year, $quarter);
    $this->start = $range['start'];
    $this->end = $range['end'];
  }

  public function getQuarterRange(int $year, int $quarter): array
  {
    $month = ($quarter - 1) * 3 + 1;
    $interval = new DateInterval('P3M');
    $format = DateTimeItemInterface::DATETIME_STORAGE_FORMAT;

    $start = DrupalDateTime::createFromArray(['year' => $year, 'month' => $month]);
    $end = $start->add($interval);

    //$period = new DatePeriod($start, $interval, $end);

    return ['start' => $start->format($format), 'end' => $end->format($format)];
  }

  public function getFields()
  {
    $investmentReviewItems = $this->getInvestmentReview();

    $fields = [
      'Quarter' => $this->getQuarterField($this->year, $this->quarter),
      'Approve Case Quantity' => $this->getApproveCaseQuantityField($investmentReviewItems),
      'Approve Amount',
      'Investor Amount',
      'Grant Amount',
      'Patent Quantity',
      'Award Quantity',
      'Employee Quantity',
      'Approve Amount Per Case',
      'Grant Amount Per Case',
      'Investment Amount Per Employee',
    ];
  }

  /**
   * @return EntityInterface[]
   */
  public function getInvestmentReview(): array
  {
    $field = 'second_review.entity:paragraph.date';

    $query = Drupal::entityQuery('node')
      ->condition('type', 'investment_review')
      ->condition('status', 1)
      ->condition($field, $this->start, '>=')
      ->condition($field, $this->end, '<')
      ->sort($field);
    $ids = $query->execute();

    if (empty($ids)) {
      return [];
    }

    try {
      return Node::loadMultiple($ids);
    } catch (Exception $error) {
      return [];
    }
  }

  /**
   * @param int $year
   * @param int $quarter
   *
   * @return string
   */
  public function getQuarterField(int $year, int $quarter): string
  {
    return "${year}Q${quarter}";
  }

  /**
   * @param EntityInterface[] $investmentReviewItems
   *
   * @return int
   */
  public function getApproveCaseQuantityField(array $investmentReviewItems): int
  {

    foreach ($investmentReviewItems as $item) {

    }
    $value = count($investmentReviewItems);

    return count($investmentReviewItems);
  }

  /**
   * @param int $node
   *
   * @return EntityInterface|null
   */
  public function getInvestmentProfile(int $node)
  {
    $field = 'date';

    $query = Drupal::entityQuery('node')
      ->condition('type', 'investment_profile')
      ->condition('status', 1)
      ->condition('investment_target.entity:node.nid', $node)
      ->condition($field, $this->end, '<')
      ->sort($field, 'DESC')
      ->pager(1);

    $ids = $query->execute();

    if (empty($ids)) {
      return null;
    }

    try {
      return Node::load(reset($ids));
    } catch (Exception $error) {
      return null;
    }
  }

  /**
   * @return array
   */
  public function getRow(): array
  {
    return $this->getInvestmentReview();

    //$items = [];
    //$items['InvestmentReview'] = $this->getInvestmentReview();
    //
    //foreach ($items['InvestmentReview'] as $item) {
    //  /** @var int $node */
    //  $node = FieldReader::create($item->getTypedData(), 'investment_target');
    //  $investment_profile = $this->getInvestmentProfile($node);
    //
    //  if (false === empty($investment_profile)) {
    //    $items['InvestmentProfile'][$node] = $investment_profile;
    //  }
    //}
    //
    //return $items;
  }
}
