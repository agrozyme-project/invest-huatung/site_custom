<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\InvestmentBenefit;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\site_custom\Form\InvestmentBenefit\InvestmentBenefitFilter;
use Drupal\site_custom\Report\Base\RowBase;
use Drupal\site_custom\Report\Base\TableReportBase;

/**
 * @class InvestmentBenefitReport
 */
class InvestmentBenefitReport extends TableReportBase
{
  public function __construct(InvestmentBenefitFilter $filter)
  {
    parent::__construct($filter);
  }

  /**
   * @return TranslatableMarkup[]
   */
  public function getTableHeader(): array
  {
    $header = [];
    $options = ['context' => 'InvestmentBenefitReport'];
    $fields = [
      '#',
      'Quarter',
      'Approve Case Quantity',
      'Approve Amount',
      'Investor Amount',
      'Grant Amount',
      'Patent Quantity',
      'Award Quantity',
      'Employee Quantity',
      'Approve Amount Per Case',
      'Grant Amount Per Case',
      'Investment Amount Per Employee',
    ];

    foreach ($fields as $field) {
      $header[$field] = t($field, [], $options);
    }

    return $header;
  }

  public function getTableOptions(): array
  {
    //$this->wrapperTableOptions();
    return [];
  }

  /**
   * @param $item
   *
   * @return RowBase
   */
  protected function getTableRow($item): RowBase
  {
    $items = $this->filter->getItems();
    $year = $items->get('year')->getItem();
    return new InvestmentBenefitRow($year);
  }
}
