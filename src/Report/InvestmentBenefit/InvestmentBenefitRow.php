<?php
declare(strict_types=1);

namespace Drupal\site_custom\Report\InvestmentBenefit;

use Drupal\site_custom\Report\Base\RowBase;

/**
 * @class InvestmentBenefitRow
 */
class InvestmentBenefitRow extends RowBase
{
  public function __construct(int $year)
  {
    $this->dataSource = new InvestmentBenefitSource($year);

    $this->fields = $this->setupFields();
  }

  protected function setupFields(): array
  {
    return [];
  }
}
