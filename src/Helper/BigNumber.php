<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper;

use Exception;
use Litipk\BigNumbers\Decimal;

/**
 * @class BigNumber
 */
class BigNumber
{
  /**
   * @param $value
   * @param int|null $scale
   *
   * @return Decimal
   */
  public static function create($value, int $scale = null): Decimal
  {
    try {
      $item = Decimal::create($value, $scale);
    } catch (Exception $exception) {
      $item = Decimal::create(0);
    }

    return $item;
  }
}
