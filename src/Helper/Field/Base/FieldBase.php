<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper\Field\Base;
/**
 * @class FieldBase
 */
abstract class FieldBase
{
  protected $format = [];
  protected $value;

  /**
   * @param mixed $value
   * @param array $format
   */
  public function __construct($value, array $format = [])
  {
    $this->value = $value;
    $this->format = $this->mergeFormat($format);
  }

  //  /**
  //   * @param mixed $value
  //   * @param array $format
  //   *
  //   * @return static
  //   */
  //  static function create($value, array $format = [])
  //  {
  //    return new static($value, $format);
  //  }

  /**
   * @param array $format
   *
   * @return array
   */
  public function mergeFormat(array $format): array
  {
    return array_merge($this->format, $format);
  }

  /**
   * @return mixed
   */
  public function getValue()
  {
    return $this->value;
  }

  /**
   * @param array $format
   *
   * @return string
   */
  public function toString(array $format = []): string
  {
    return strval($this->value);
  }
}
