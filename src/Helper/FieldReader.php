<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper;

use Drupal\Core\TypedData\ComplexDataInterface;
use Exception;

/**
 * @class FieldReader
 */
class FieldReader
{
  protected $data;
  protected $delta = 0;
  protected $index = '';
  protected $name = '';

  /**
   * @param ComplexDataInterface $data
   * @param string $name
   * @param string $index
   * @param int $delta
   */
  protected function __construct(ComplexDataInterface $data, string $name, string $index = '', int $delta = 0)
  {
    $this->data = $data;
    $this->name = $name;
    $this->index = $index;
    $this->delta = $delta;
  }

  /**
   * @param ComplexDataInterface $data
   * @param string $name
   * @param string $index
   * @param int $delta
   *
   * @return static
   */
  public static function create(
    ComplexDataInterface $data,
    string $name,
    string $index = '',
    int $delta = 0
  ): FieldReader {
    return new static($data, $name, $index, $delta);
  }

  /**
   * @param FieldReader[] $items
   *
   * @return array
   */
  public static function getValues(array $items = []): array
  {
    $data = [];

    foreach ($items as $item) {
      $data[$item->getName()] = $item->getValue();
    }

    return $data;
  }

  /**
   * @return string
   */
  public function getName(): string
  {
    return $this->name;
  }

  /**
   * @return mixed
   */
  public function getValue()
  {
    $index = $this->index;

    try {
      $value = $this->data->get($this->name)->getValue();
    } catch (Exception $exception) {
      return null;
    }

    $value = $value[$this->delta] ?? null;

    if ('' === $index) {
      return $value;
    }

    return $value[$index] ?? null;
  }

  /**
   * @return bool
   */
  public function isEmpty(): bool
  {
    $value = $this->getValue();
    return isset($value);
  }
}
