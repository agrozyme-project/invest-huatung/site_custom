<?php
declare(strict_types=1);

namespace Drupal\site_custom\Helper;

use DateInterval;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * @class Quarter
 */
class Quarter
{
  protected $end;
  protected $quarter;
  protected $start;
  protected $year;

  public function __construct(int $year, int $quarter)
  {
    $this->year = $year;
    $this->quarter = $quarter;
  }

  /**
   * @return DrupalDateTime
   */
  public function getEnd(): DrupalDateTime
  {
    return $this->end;
  }

  /**
   * @return int
   */
  public function getQuarter(): int
  {
    return $this->quarter;
  }

  /**
   * @return DrupalDateTime
   */
  public function getStart(): DrupalDateTime
  {
    return $this->start;
  }

  /**
   * @return int
   */
  public function getYear(): int
  {
    return $this->year;
  }

  protected function setDateRange()
  {
    $month = ($this->quarter - 1) * 3 + 1;
    $this->start = DrupalDateTime::createFromArray(['year' => $this->year, 'month' => $month]);
    $this->end = $this->start->add(new DateInterval('P3M'));
  }
}
