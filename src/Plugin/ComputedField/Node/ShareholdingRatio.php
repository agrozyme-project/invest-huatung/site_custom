<?php
declare(strict_types=1);

namespace Drupal\site_custom\Plugin\ComputedField\Node;

use Drupal\computed_field_plugin\Traits\ComputedSingleItemTrait;
use Drupal\Core\Field\FieldItemList;
use Drupal\site_custom\Helper\BigNumber;

/**
 * @class ShareholdingRatio
 * @ComputedField(
 *   id = "shareholding_ratio_field",
 *   label = @Translation("Shareholding Ratio"),
 *   type = "computed_render_array",
 *   entity_types = {"node"},
 *   bundles = {"investment_review"}
 * )
 */
class ShareholdingRatio extends FieldItemList
{
  use ComputedSingleItemTrait;

  /**
   * {@inheritdoc}
   */
  protected function singleComputeValue(): array
  {
    $entity = $this->getEntity();

    $quantity = BigNumber::create($entity->get('shareholding_quantity')->getString());
    $total = BigNumber::create($entity->get('total_share')->getString());

    if ($total->isZero()) {
      $data = BigNumber::create(0);
    } else {
      $data = $quantity->div($total);
    }

    return ['#markup' => $data->asFloat()];
  }
}
